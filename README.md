A console application that receives input from the user and stores it in a collection that can hold any data types.The types int, double, string, char and bool are supported.

The collection stores data on random indexes and uses probing if an index is occupied. 
A user can print out the collection to see the various indexers that can be tasked with retrieving an item

Custom exeptions and error handling ensure that a user cannot add items if the list is full, or print and retrieve when it is full