﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task9_SkjelinOttosen
{
    class ChaosCollection<T> : ICollection<T>
    {
        int count;
        public Object[] ChaosArray { get; set; }

        public int Count => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public ChaosCollection(int size)
        {
            count = 0;
            ChaosArray = new Object[size];
        }

        // Checks if collection is empty
        public bool IsEmpty()
        {
            if(count < 1)
            {
                return true;
            }
            return false;
        }

        // Add method for the collection
        public void Add(T item)
        {
            // Generates random index
            var random = new Random();
            int lastIndex = ChaosArray.Length;
            int randomIndex = random.Next(0, lastIndex);
            int next = randomIndex;

            // Throws exception if the collection is full
            if(count >= ChaosArray.Length)
            {
                throw new CollectionIsFullException("The collection is full");
            }
            else
            {
                // Checks if array[index] is null
                while (ChaosArray[next] != null)
                {
                    next++;
                    randomIndex++;

                    //Wrap around to index 0
                    if (next >= lastIndex)
                    {
                        next %= lastIndex;
                    }
                    //Second iteration of array
                    if (next == lastIndex)
                    {
                        throw new CollectionIsFullException("The collection is full");
                    }
                }
                // Inserts value
                ChaosArray[next] = item;
                count++;
            }       
        }

        public object GetValueAtIndex(int index)
        {
            // Checks if the collection is not empty
            if (!IsEmpty()) 
            {
                return ChaosArray[index];
            }
            // Throws an exception if the collection is empty
            else
            {
                throw new DeleteEmptyCollectionException("Collection is empty");
            }       
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        // Remove method for the collection
        public object Remove()
        {
             // Generates random index
            var random = new Random();
            int lastIndex = ChaosArray.Length;
            int randomIndex = random.Next(0, lastIndex);
            int next = randomIndex;

            
            // Checks is collection is empty
            if (IsEmpty())
            {
                throw new DeleteEmptyCollectionException("Collection is empty");
            }
            // Throws an exception if the collection is empty
            else
            {
                // Checks if array[index] has a value 
                while (ChaosArray[next] == null)
                {
                    next++;
                    randomIndex++;

                    //Wrap around to index 0
                    if (next >= lastIndex)
                    {
                        next %= lastIndex;
                    }
                    //Second iteration of array
                    if (next == lastIndex)
                    {
                        throw new DeleteEmptyCollectionException("Collection is empty");
                    }
                }
                // Inserts value
                object tmp = ChaosArray[next];
                ChaosArray[next] = null;
                count--;
                return tmp;
            }
        }

        //Print method for the collection
        public void Print() 
        {
            // Checks is collection is empty
            if (!IsEmpty())
            {
                int index = 0;
                foreach (Object element in ChaosArray)
                {
                    Console.WriteLine($"{index++} {element}");
                }
                Console.WriteLine();
            }
            // Throws an exception if the collection is empty
            else
            {
                throw new DeleteEmptyCollectionException("Collection is empty");
            }       
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }
    }
}
