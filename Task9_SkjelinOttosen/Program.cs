﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Linq;
using System.Numerics;

namespace Task9_SkjelinOttosen
{
    class Program
    {
        static void Main(string[] args)
        {
            // Collection that can take int, doubles, strings and booleans.
            ChaosCollection<Object> collection = new ChaosCollection<Object>(10); 
            // Runs until the user exits the program
            while (true)
            {
                try
                {
                    // Set the Foreground color to white
                    Console.ForegroundColor = ConsoleColor.White;
                    
                    //Menu for the console app
                    Console.WriteLine("Chaos Collection");
                    Console.WriteLine("Press 1 and enter to insert values");
                    Console.WriteLine("Press 2 and enter to delete random value from the collection");
                    Console.WriteLine("Press 3 and enter to print the collection");
                    Console.WriteLine("Press 4 and enter to get a value from the collection");

                    // Menu input for the application
                    int input = Int16.Parse(Console.ReadLine());
                   
                    // Checks menu input from the user
                    // Insert method
                    if (input == 1)
                    {
                        Console.WriteLine("Insert:");

                        // Tries to parse input into to data types
                        string inputString = Console.ReadLine();
                        bool isCharacter = Char.TryParse(inputString, out char character);
                        bool isInputInteger = Int32.TryParse(inputString, out int intNumber);
                        bool isInputDouble = Double.TryParse(inputString, out double doubleNumber);
                        bool isBoolean = bool.TryParse(inputString, out bool boolean);
                        

                        // Set the Foreground color to dark yellow
                        Console.ForegroundColor = ConsoleColor.DarkYellow;

                        // Checks datatypes
                        if (isInputInteger)
                        {
                            int inputInt = Int32.Parse(inputString);
                            collection.Add(inputInt);
                            Console.WriteLine($"The value {inputInt} was successfully added to the collection as a {inputInt.GetType().Name}.\n");
                        }
                        else if(isInputDouble)
                        {
                            double inputDouble = Double.Parse(inputString);
                            collection.Add(inputDouble);
                            Console.WriteLine($"The value {inputDouble} was successfully added to the collection as a {inputDouble.GetType().Name}.\n");

                        }
                        else if(isBoolean)
                        {
                            bool inputBoolean = bool.Parse(inputString);
                            collection.Add(inputBoolean);
                            Console.WriteLine($"The value {inputBoolean} was successfully added to the collection as a {inputBoolean.GetType().Name}.\n");

                        }
                        else if (isCharacter)
                        {
                            char inputCharacter = char.Parse(inputString);
                            collection.Add(inputCharacter);
                            Console.WriteLine($"The value {inputCharacter} was successfully added to the collection as a {inputCharacter.GetType().Name}.\n");
                        }
                        else
                        {
                            collection.Add(new String(inputString));
                            Console.WriteLine($"The value {inputString} was successfully added to the collection as a {inputString.GetType().Name}.\n");
                        }                    
                    }
                    // Delete method
                    else if (input == 2)
                    {
                        // Set the Foreground color to dark red
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine($"{collection.Remove()} was deleted from the collection.\n");            
                    }
                    // Print method
                    else if (input == 3)
                    {
                        // Set the Foreground color to dark yellow
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        collection.Print();
                    }
                    // Get value at a given index
                    else if (input == 4)
                    {
                        // Set the Foreground color to dark yellow
                        Console.ForegroundColor = ConsoleColor.DarkYellow;

                        // Prints collection to see the indexes to choose from
                        collection.Print();

                        // Set the Foreground color to white
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("Enter a index to get the value from:");
                        int inputIndex = Int16.Parse(Console.ReadLine());

                        // Set the Foreground color to dark yellow
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        object value = collection.GetValueAtIndex(inputIndex);
                        Console.WriteLine($"Returned {value} as a {value.GetType().Name}.\n");
                    }
                }
                catch(DeleteEmptyCollectionException ex)
                {
                    // Set the Foreground color to dark red
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
                catch (CollectionIsFullException ex)
                {
                    // Set the Foreground color to dark red
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
                catch (Exception ex)
                {
                    // Set the Foreground color to dark red
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
            }
        }
    }
}
