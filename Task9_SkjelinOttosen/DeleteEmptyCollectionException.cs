﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9_SkjelinOttosen
{
    [Serializable]
    class DeleteEmptyCollectionException : Exception
    {
        public DeleteEmptyCollectionException(string message)
        : base(message)
        {

        }
    }
}
