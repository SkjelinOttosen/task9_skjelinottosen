﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9_SkjelinOttosen
{
    [Serializable]
    class CollectionIsFullException : Exception
    {   
        public CollectionIsFullException(string message)
        : base(message)
        {

        }
    }
}
